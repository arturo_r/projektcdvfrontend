import React, { Component } from 'react';
import Helmet from 'react-helmet';
import '../App.css';
import { Link } from 'react-router-dom';

// Redux imports
import { connect } from "react-redux";
import { set_age } from "../action";

import logo0 from '../img/monster-0.png';
import logo1 from '../gifs/01.gif';
import logo2 from '../gifs/02.gif';
import logo3 from '../gifs/03.gif';
import logo4 from '../gifs/04.gif';

import arrow_left from '../img/arrow-left.png';
import arrow_right from '../img/arrow-right.png';

class Step1 extends Component {
    state = {
        number: this.props.age
    };

    defineImageToAge(age){
        if(age > 0 && age <= 6){
            return 1;
        }
        else if(age >= 7 && age <= 11){
            return 2;
        }
        else if(age > 11 && age < 18){
            return 3;
        }
        else if(age >= 18){
            return 4;
        }
        else return 0; 
    } 

    selectedMonster() {
        let number = this.defineImageToAge(this.state.number);

        switch (number) {
            case 1:
            return <div className="monster-block monster-block-selected" id="monster-1"><img src={logo1} alt={"monster-age-" + number} className="monster-img" /></div>

            case 2:
            return <div className="monster-block monster-block-selected" id="monster-2"><img src={logo2} alt={"monster-age-" + number} className="monster-img" /></div>

            case 3:
            return <div className="monster-block monster-block-selected" id="monster-3"><img src={logo3} alt={"monster-age-" + number} className="monster-img" /></div>

            case 4:
            return <div className="monster-block monster-block-selected" id="monster-4"><img src={logo4} alt={"monster-age-" + number} className="monster-img" /></div>
            default:
                 return <div className="monster-block monster-block-selected" id="monster-4"><img src={logo0} alt={"monster-age-" + number} className="monster-img" /></div>
        }
 
    }

    render() {
        return (
            <div className="Step1">
                <Helmet bodyAttributes={{
                    style: 'background-color : #133d4e'
                }} />
                <form id="age-form">
                    <p className="question-first">Wiek</p>
                    <div className="arrows-container">
                        <small>
                            Wpisz swój wiek i sprawdź, którym z monsterków <br />

                            jesteś. Wiek powinien być poprawny, pomoże nam <br />
                            to w doborze odpowiedniego filmu dla Ciebie.
                        </small>
                        <Link to={{ pathname: '/step2', answer1: this.props.age }}>
                            <div className="arrow-next" >
                                <img src={arrow_right} alt="" className="arrow arrow-right" />
                            </div>
                        </Link>
                    </div>
                    <input 
                        type="number" 
                        className="age-field"
                        defaultValue={this.state.number} 
                        placeholder="..."
                        onChange={ 
                            e => {
                                this.setState({number: e.target.value});
                                this.props.setAge(e.target.value);
                            }
                        }
                    />
                </form>
                <div className="monsters-container">
                    {this.selectedMonster()}
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        age: state.age
    }
}

const mapDispatchToProps = {
    setAge: set_age
}

export default connect(mapStateToProps, mapDispatchToProps)(Step1);
