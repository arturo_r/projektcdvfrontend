import React, { Component } from 'react';
import Helmet from 'react-helmet';
import logo from '../img/Asset 1@4x-8.png';
import classnames from 'classnames';
import '../App.css';
import './MainSite.css';
import monster from '../img/MainSite/monster.png'


class MainSite extends Component {
    state = {
        active: false,
        redirect: false
    };

    setRedirect = () => {
        this.setState({
            active: true,
            redirect: true
        })
    }
    redirectToTarget = () => {
        this.props.history.push('/step1')
    }
    render() {
        let classes = classnames('btn start-btn ', { active: this.state.active });
        return (

            <div id="container">
                <header id="header-main-site">
                    <a href="/">
                        <img src={logo} alt="logo" className="logo" />
                    </a>
                </header>
                <div className="MainSite">
                    <Helmet bodyAttributes={{
                        style: 'background-color : #133d4e'
                    }} />


                    <div id="text-info">
                        <p id="head">monsvie</p>
                        <p id="second-head">Idealny film na dziś</p>
                        <p id="main-text">Zrób szybki test z monsvie i znajdź idelany film dla <br />
                            siebie! Wystarczą tylko trzy kliknięcia, aby otrzymać <br />
                            propozycję dwóch filmów dobranych indywidualnie <br />
                            dla Twojej osoby.
                        </p>
                        <button className={classes} onClick={this.redirectToTarget}>rozpocznij test</button>
                    </div>
                    <div>
                        <img id="monster-image" src={monster} alt="monster" />
                    </div>

                </div>

            </div>
        );
    }
}

export default MainSite;
