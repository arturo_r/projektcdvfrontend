import React, { Component } from 'react';
import './App.css';
import MainSite from './MainSite/MainSite';
import Step1 from './Step1/Step1';
import Step2 from './Step2/Step2';
import Step3 from './Step3/Step3';
import ResultSteps from './ResultSteps/ResultSteps';
import { BrowserRouter, Switch, Route } from 'react-router-dom';

class App extends Component {
	render() {
		return (
			<div className="App">
				<div className="site-content">
					<BrowserRouter>
						<Switch>
							<Route exact path='/' component={MainSite} />
							<Route path='/step1' component={Step1} />
							<Route path='/step2' component={Step2} />
							<Route path='/step3' component={Step3} />
							<Route path='/result-steps' component={ResultSteps} />
						</Switch>
					</BrowserRouter>
				</div>
			</div>
		);
	}
}

export default App;
