import React, { Component } from 'react';
import Helmet from 'react-helmet';
import '../App.css';
import { Link } from 'react-router-dom';
import adult1 from '../img/step3/dorosly1.png';
import adult2 from '../img/step3/dorosly2.png';
import adult3 from '../img/step3/dorosly3.png';
import adult4 from '../img/step3/dorosly4.png';
import baby1 from '../img/step3/dziecko1.png';
import baby2 from '../img/step3/dziecko2.png';
import baby3 from '../img/step3/dziecko3.png';
import baby4 from '../img/step3/dziecko4.png';
import teenager1 from '../img/step3/mlody1.png';
import teenager2 from '../img/step3/mlody2.png';
import teenager3 from '../img/step3/mlody3.png';
import teenager4 from '../img/step3/mlody4.png';
import elderly1 from '../img/step3/najstarszy1.png';
import elderly2 from '../img/step3/najstarszy2.png';
import elderly3 from '../img/step3/najstarszy3.png';
import elderly4 from '../img/step3/najstarszy4.png';
import arrow_left from '../img/arrow-left.png';
import arrow_right from '../img/arrow-right.png';

// Redux imports
import { connect } from "react-redux";
import { set_mood } from "../action";

class Step3 extends Component {
	state = {
		mood: this.props.mood
	};

	setMoodSmile = event => {
		this.setState({
			mood: 'smile'
		});
	}

	setMoodTouch = event => {
		this.setState({
			mood: 'touch'
		});
	}

	setMoodSurprise = event => {
		this.setState({
			mood: 'surprise'
		});
	}

	setMoodScary = event => {
		this.setState({
			mood: 'scary'
		});
	}

	render() {
		const age = this.props.location.answer1;

		return (
			<div className="step3">
				<Helmet bodyAttributes={{
					style: 'background-color : #133d4e'
				}} />
				<p className="question-second">Wybierz nastrój</p>
				<div className="arrows-container">
					<Link to='/step2'>
						<div className="arrow-prev">
							<img src={arrow_left} alt="" className="arrow arrow-left" />
						</div>
					</Link>
					<small>Wybierz nastrój jaki chciałbyś/abyś mieć<br />
						po obejrzeniu filmu.</small>
					<Link to={{ pathname: '/result-steps', answer1: this.props.age, answer2: this.props.movie_age, answer3: this.props.mood }}>
						<div className="arrow-next">
							<img src={arrow_right} alt="" className="arrow arrow-right" />
						</div>
					</Link>
				</div>
				<div className="monsters-container">
					<div className="monster-block" id="monster-1">
						{age <= 6 ? (<img src={baby1} alt="monster-age-1" className="monster-img" />) : (null)}
						{age > 6 && age <= 11 ? (<img src={teenager1} alt="monster-age-1" className="monster-img" />) : (null)}
						{age >= 12 && age < 18 ? (<img src={adult1} alt="monster-age-1" className="monster-img" />) : (null)}
						{age >= 18 ? (<img src={elderly1} alt="monster-age-1" className="monster-img" />) : (null)}
						<button onClick={() => this.props.set_mood('scary')} className={"btn " + (this.props.mood === 'scary' ? 'active' : '')}>chcę się wystraszyć</button>
					</div>
					<div className="monster-block" id="monster-2">
						{age <= 6 ? (<img src={baby2} alt="monster-age-2" className="monster-img" />) : (null)}
						{age > 6 && age <= 11 ? (<img src={teenager2} alt="monster-age-2" className="monster-img" />) : (null)}
						{age >= 12 && age < 18 ? (<img src={adult2} alt="monster-age-2" className="monster-img" />) : (null)}
						{age >= 18 ? (<img src={elderly2} alt="monster-age-2" className="monster-img" />) : (null)}
						<button onClick={() => this.props.set_mood('touch')} className={"btn " + (this.props.mood === 'touch' ? 'active' : '')}>chcę się wzruszyć</button>
					</div>
					<div className="monster-block" id="monster-3">
						{age <= 6 ? (<img src={baby3} alt="monster-age-3" className="monster-img" />) : (null)}
						{age > 6 && age <= 11 ? (<img src={teenager3} alt="monster-age-3" className="monster-img" />) : (null)}
						{age >= 12 && age < 18 ? (<img src={adult3} alt="monster-age-3" className="monster-img" />) : (null)}
						{age >= 18 ? (<img src={elderly3} alt="monster-age-3" className="monster-img" />) : (null)}
						<button onClick={() => this.props.set_mood('smile')} className={"btn " + (this.props.mood === 'smile' ? 'active' : '')}>chcę się pośmiać</button>
					</div>
					<div className="monster-block" id="monster-4">
						{age <= 6 ? (<img src={baby4} alt="monster-age-4" className="monster-img" />) : (null)}
						{age > 6 && age <= 11 ? (<img src={teenager4} alt="monster-age-4" className="monster-img" />) : (null)}
						{age >= 12 && age < 18 ? (<img src={adult4} alt="monster-age-4" className="monster-img" />) : (null)}
						{age >= 18 ? (<img src={elderly4} alt="monster-age-4" className="monster-img" />) : (null)}
						<button onClick={() => this.props.set_mood('surprise')} className={"btn " + (this.props.mood === 'surprise' ? 'active' : '')}>chcę się zaskoczyć</button>
					</div>
				</div>
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		movie_age: state.movie_age,
		age: state.age,
		mood: state.mood
	}
}

const mapDispatchToProps = {
	set_mood: set_mood
}

export default connect(mapStateToProps, mapDispatchToProps)(Step3);
