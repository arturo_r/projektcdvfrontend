import { SET_AGE, SET_MOOD, SET_MOVIE_AGE, CLEAR_STATE } from "../action";

const initialState= {
    age: '',
    mood: '',
    movie_age: ''
}

export const reducer = (state=initialState, action) => {
    switch(action.type){
        case SET_AGE:
            return {
                ...state,
                age: action.payload
            };
        case SET_MOOD:
            return {
                ...state,
                mood: action.payload
            };
        case SET_MOVIE_AGE:
            return {
                ...state,
                movie_age: action.payload
            };
        case CLEAR_STATE:
            return {
                age: '',
                mood: '',
                movie_age: ''
            }
        default:
            return state;
    }
}