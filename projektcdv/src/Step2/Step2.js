import React, { Component } from 'react';
import Helmet from 'react-helmet';
import '../App.css';
import { Link } from 'react-router-dom';
import logo1 from '../img/step2/01.png';
import logo2 from '../img/step2/02.png';
import logo3 from '../img/step2/03.png';
import logo4 from '../img/step2/04.png';
import arrow_left from '../img/arrow-left.png';
import arrow_right from '../img/arrow-right.png';
import timeline from '../img/step2/time-line.png';

// Redux imports
import { connect } from "react-redux";
import { set_movie_age } from "../action";


class Step2 extends Component {
	
	state = {
		ageMovie: this.props.movie_age
	};

	setMyYear() {
		let currentYear = new Date().getFullYear();
		let myYearOfBirth = currentYear - this.props.age;
		return myYearOfBirth;
	}

	defineImageToAge(age){
        if(age > 0 && age <= 6){
            return 1;
        }
        else if(age >= 7 && age <= 11){
            return 2;
        }
        else if(age > 11 && age < 18){
            return 3;
        }
        else if(age >= 18){
            return 4;
        }
        else return 0; 
    } 

	selectedMonster() {
		let number = this.defineImageToAge(this.props.age);
		switch (number) {
			case 1:
				return <div className="monster-block monster-block-selected" id="monster-1"><img src={logo1} alt={"monster-age-" + number} className="monster-img" /></div>
			case 2:
				return <div className="monster-block monster-block-selected" id="monster-2"><img src={logo2} alt={"monster-age-" + number} className="monster-img" /></div>
			case 3:
				return <div className="monster-block monster-block-selected" id="monster-3"><img src={logo3} alt={"monster-age-" + number} className="monster-img" /></div>
			case 4:
				return <div className="monster-block monster-block-selected" id="monster-4" > <img src={logo4} alt={"monster-age-" + number} className="monster-img" /></div >
			default:
				return null;
		}
	};

	render() {
		return (
			<div>
				<Helmet bodyAttributes={{
					style: 'background-color : #133d4e'
				}} />
				<p className="question-second">Oś czasu</p>
				<div className="arrows-container">
					<Link to='/step1'><div className="arrow-prev"><img src={arrow_left} alt="" className="arrow arrow-left" /></div></Link>
					<small>
						Na osi czasu zaznacz jak stary <br />
						film chciałbyś/abyś obejrzeć. Do <br />
						wyboru masz pięć kategorii.
					</small>
					<Link to={{ pathname: '/step3', answer1: this.props.age, answer2: this.props.movie_age }}>
						<div className="arrow-next">
							<img src={arrow_right} alt="" className="arrow arrow-right" />
						</div>
					</Link>
				</div>
				<div className="monsters-step-second-container">
					{this.selectedMonster()}
				</div>
				<div className="step-second-container">
					<button onClick={() => this.props.set_movie_age('black-white')} className={"btn " + (this.props.movie_age === 'black-white' ? 'active' : '')}>czarno-białe</button>
					<button onClick={() => this.props.set_movie_age('before my birth')} className={"btn " + (this.props.movie_age === 'before my birth' ? 'active' : '')}>przed moim <br /> urodzeniem</button>
					<button onClick={() => this.props.set_movie_age('my-year')} className={"btn " + (this.props.movie_age === 'my-year' ? 'active' : '')} >z mojego <br /> roku urodzenia</button>
					<button onClick={() => this.props.set_movie_age('a little younger')} className={"btn " + (this.props.movie_age === 'a little younger' ? 'active' : '')}>trochę młodszy <br /> ode mnie</button>
					<button onClick={() => this.props.set_movie_age('contemporary')} className={"btn " + (this.props.movie_age === 'contemporary' ? 'active' : '')}>współczesne</button>
				</div>
				<div className="time-line">
					<img className="time-line" alt="time-line" src={timeline} />
					<p id="myYear">{this.setMyYear()}</p>
				</div>
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		movie_age: state.movie_age,
		age: state.age
	}
}

const mapDispatchToProps = {
	set_movie_age: set_movie_age
}

export default connect(mapStateToProps, mapDispatchToProps)(Step2);
